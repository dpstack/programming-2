# Proyecto de Programación 2

## Descripción General
Este repositorio contiene soluciones y recursos para una serie de laboratorios de la asignatura Programación 2. El proyecto está estructurado para facilitar el aprendizaje y la práctica de conceptos avanzados de programación en Java.

## Estructura del Proyecto
```
programming-2/
├── app/
│   └── src/
│       └── main/
│           └── java/
│               └── labs/
│                   ├── lab1/
│                   ├── lab2/
│                   ├── lab3/
│                   └── App.java
├── gradle/
├── .idea/
├── .gitattributes
├── .gitignore
├── .gitlab-ci.yml
├── LICENSE
├── README.md
├── gradlew
├── gradlew.bat
└── settings.gradle
```

## Laboratorios
1. **Lab 1**: Inicialización y conceptos básicos POO y Procedural
2. **Lab 2**: Pilares de POO
3. **Lab 3**: Clases Abstractas, Interfaces, Enums

Cada laboratorio se encuentra en su respectiva carpeta dentro de `app/src/main/java/labs/`.

## Diagrama de Clases
El diagrama de clases para la Semana 3 se encuentra en la carpeta `lab3`. Este diagrama proporciona una visión estructural de las clases utilizadas en el tercer laboratorio.

## Archivo Principal
`App.java` No contiene información primordial.

## Compilación y Ejecución
Este proyecto utiliza Gradle como herramienta de construcción. Para compilar y ejecutar el proyecto:

1. Asegúrate de tener Java instalado en tu sistema.
2. Abre una terminal en el directorio raíz del proyecto.
3. Ejecuta `./gradlew build` (Unix) o `gradlew.bat build` (Windows) para compilar el proyecto.
4. Ejecuta `./gradlew run` (Unix) o `gradlew.bat run` (Windows) para ejecutar la aplicación.

## Licencia
Consulta el archivo `LICENSE` para obtener detalles sobre la licencia del proyecto.

## Contribuciones
Por favor, lee el archivo `.gitlab-ci.yml` para obtener información sobre el pipeline de CI/CD y las pautas de contribución.

## Información Adicional
- El proyecto está configurado para ser utilizado con IntelliJ IDEA, como se indica por la presencia del directorio `.idea/`.
- Se utiliza GitLab para el control de versiones.
