package labs.lab2;

import java.time.LocalDate;

public interface IPersona {
    String getNombre();
    String getApellido();
    LocalDate getFechaNacimiento();
    String getTelefono();
    String getEmail();
}
