package labs.lab2;

import java.time.LocalDate;

public class EntradaHistorial {

    private LocalDate fecha;
    private String desc;
    private Medico medico;

    public EntradaHistorial(LocalDate fecha, String desc, Medico medico) {
        this.fecha = fecha;
        this.desc = desc;
        this.medico = medico;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public String getDesc() {
        return desc;
    }

    public Medico getMedico() {
        return medico;
    }
}
