package labs.lab2;

import java.util.ArrayList;
import java.util.List;

public class HistorialMedico {

    private List<EntradaHistorial> entradas;

    public HistorialMedico() {
        this.entradas = new ArrayList<>();
    }

    public void addEntrada(EntradaHistorial entrada) {
        entradas.add(entrada);
    }

    public List<EntradaHistorial> getHistorial() {
        return entradas;
    }
}
