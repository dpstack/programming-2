package labs.lab2;

import java.time.LocalDate;

public class AbstractPersonaBase implements IPersona {
    protected String nombre;
    protected String apellido;
    protected LocalDate fechaNacimiento;
    protected String telefono;
    protected String email;

    public AbstractPersonaBase(String nombre, String apellido, LocalDate fechaNacimiento, String telefono, String email) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacimiento = fechaNacimiento;
        this.telefono = telefono;
        this.email = email;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public String getApellido() {
        return apellido;
    }

    @Override
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    @Override
    public String getTelefono() {
        return telefono;
    }

    @Override
    public String getEmail() {
        return email;
    }
}
