package labs.lab2;

import java.time.LocalDate;

// Herencia
public class Medico extends AbstractPersonaBase {

    private String especialidad;
    private Horario horario;

    public Medico(String nombre, String apellido, LocalDate fechaNacimiento, String telefono, String email, String especialidad) {
        super(nombre, apellido, fechaNacimiento, telefono, email);
        this.especialidad = especialidad;
        this.horario = new Horario();
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public Horario getHorario() {
        return horario;
    }
}
