package labs.lab2;

import java.time.LocalDateTime;

public class Cita implements ICitaGestionable {

    private Paciente paciente;
    private Medico medico;
    private LocalDateTime fecha;
    private String estado;

    public Cita(Paciente paciente, Medico medico, LocalDateTime fecha) {
        this.paciente = paciente;
        this.medico = medico;
        this.fecha = fecha;
        this.estado = "Programada";
    }

    @Override
    public void cancelar() {
        this.estado = "Cancelada";
    }

    @Override
    public void reprogramar(LocalDateTime fechaReprogramada) {
        this.fecha = fechaReprogramada;
        this.estado = "Reprogramada";
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public String getEstado() {
        return estado;
    }
}
