package labs.lab2;

import java.time.LocalDateTime;
// Segregación de Interfaces
public interface ICitaGestionable {
    void cancelar();
    void reprogramar(LocalDateTime fechaReprogramada);
}
