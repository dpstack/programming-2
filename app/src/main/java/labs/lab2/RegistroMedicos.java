package labs.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RegistroMedicos implements IRegistro<Medico> {

    private List<Medico> medicos;

    public RegistroMedicos() {
        this.medicos = new ArrayList<>();
    }

    @Override
    public void agregar(Medico medico) {
        medicos.add(medico);
    }

    // Busqueda por nombre
    @Override
    public Optional<Medico> buscar(String nombre) {
        return medicos.stream()
                .filter(med -> med.getNombre().equals(nombre))
                .findFirst();
    }

    public List<Medico> filtrarPorEspecialidad(String especialidad) {
        return medicos.stream()
                .filter(med -> med.getEspecialidad().equals(especialidad))
                .collect(Collectors.toList());
    }

}
