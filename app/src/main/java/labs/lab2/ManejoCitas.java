package labs.lab2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ManejoCitas {

    private List<Cita> citas;

    public ManejoCitas() {
        this.citas = new ArrayList<>();
    }

    public Optional<Cita> programarCita(Paciente paciente, Medico medico, LocalDateTime fecha) {
        if (medico.getHorario().isDisponible(fecha)) {
            Cita cita = new Cita(paciente, medico, fecha);
            citas.add(cita);
            return Optional.of(cita);
        }

        return Optional.empty();
    }

    public List<Cita> buscarCitasPaciente(Paciente paciente) {
        return citas.stream()
                .filter(cita -> cita.getPaciente().equals(paciente))
                .collect(Collectors.toList());
    }

    public boolean cancelarCita(Cita cita) {
        if (citas.contains(cita)) {
            cita.cancelar();
            return true;
        }
        return false;
    }
}
