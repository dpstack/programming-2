package labs.lab2;

import java.util.Optional;

// Intefaz genérica, OCP
public interface IRegistro<T> {

    void agregar(T item);
    Optional<T> buscar(String id);

}
