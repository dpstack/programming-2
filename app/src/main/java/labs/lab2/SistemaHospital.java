package labs.lab2;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Optional;

public class SistemaHospital {
    // Inversión de Dependencias
    private final RegistroPacientes registroPacientes;
    private final RegistroMedicos registroMedicos;
    private final ManejoCitas manejoCitas;
    private final Facturacion facturacion;

    public SistemaHospital() {
        this.registroPacientes = new RegistroPacientes();
        this.registroMedicos = new RegistroMedicos();
        this.manejoCitas = new ManejoCitas();
        this.facturacion = new Facturacion();
    }

    public void registrarPaciente(Paciente paciente) {
        registroPacientes.agregar(paciente);
    }

    public void registrarMedico(Medico medico) {
        registroMedicos.agregar(medico);
    }

    public Optional<Cita> agendarCita(String seguroPaciente, String nombreMedico, LocalDateTime fecha) {
        Optional<Paciente> paciente = registroPacientes.buscar(seguroPaciente);
        Optional<Medico> medico = registroMedicos.buscar(nombreMedico);

        if (paciente.isPresent() && medico.isPresent()) {
            return manejoCitas.programarCita(paciente.get(), medico.get(), fecha);
        }

        return Optional.empty();
    }

    public Optional<Factura> generarFactura(Cita cita, double monto) {
        return Optional.of(facturacion.generarFactura(cita, monto));
    }

    public static void main(String[] args) {
        SistemaHospital sistemaHospital = new SistemaHospital();

        // Registrar Médico
        Medico doctorDaniel = new Medico("Daniel", "Pérez", LocalDate.of(2000, 7, 12), "3148563399", "doctor_daniel@gmail.com", "Dermatólogo");
        doctorDaniel.getHorario().setDisponibilidad(DayOfWeek.SATURDAY, Arrays.asList(
                LocalTime.of(7, 0),
                LocalTime.of(8,0),
                LocalTime.of(9,0)
        ));
        sistemaHospital.registrarMedico(doctorDaniel);

        // Registrar Paciente
        Paciente pacienteHumberto = new Paciente("Humberto", "Benavides", LocalDate.of(1980, 2, 27), "3114391188", "humberto_ben11@gmail.com", "AT955873");
        sistemaHospital.registrarPaciente(pacienteHumberto);

        // Agendar Cita
        LocalDateTime fechaCita = LocalDateTime.of(2024, 7, 13, 8, 0);
        Optional<Cita> cita = sistemaHospital.agendarCita(pacienteHumberto.getSeguro(), doctorDaniel.getNombre(), fechaCita);

        cita.ifPresentOrElse(c -> {
            System.out.println("Cita agendada de " + c.getPaciente().getNombre() + " con el Doctor " + c.getMedico().getApellido() + " el día " + c.getFecha());
            // Generar factura
            Optional<Factura> factura = sistemaHospital.generarFactura(c, 12000.00);
            factura.ifPresent(f ->
                System.out.println("Factura generada con el monto de $" + f.getMonto() + " con la fecha de " + f.getCita().getFecha())
            );
        }, () -> System.out.println("Hubo un error agendando la cita"));
    }

}
