package labs.lab2;

public interface ICoberturaMedica {

    boolean hayCobertura(String servicio);

}
