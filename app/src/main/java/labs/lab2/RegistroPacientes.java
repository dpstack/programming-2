package labs.lab2;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class RegistroPacientes implements IRegistro<Paciente> {

    private Map<String, Paciente> pacientes;

    public RegistroPacientes() {
        this.pacientes = new HashMap<>();
    }

    // Filtrar por seguro
    @Override
    public void agregar(Paciente paciente) {
        pacientes.put(paciente.getSeguro(), paciente);
    }

    @Override
    public Optional<Paciente> buscar(String seguro) {
        return Optional.ofNullable(pacientes.get(seguro));
    }
}
