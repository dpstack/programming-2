package labs.lab2;

import java.util.ArrayList;
import java.util.List;

public class Facturacion {

    private List<Factura> facturas;

    public Facturacion() {
        this.facturas = new ArrayList<>();
    }

    public Factura generarFactura(Cita cita, double monto) {
        Factura factura = new Factura(cita, monto);
        facturas.add(factura);
        return factura;
    }

}
