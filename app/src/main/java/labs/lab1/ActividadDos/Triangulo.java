package labs.lab1.ActividadDos;

public class Triangulo implements Figura {
    private double primerLado;
    private double segundoLado;
    private double tercerLado;

    // Constructor
    public Triangulo(double primerLado, double segundoLado, double tercerLado) {
        this.primerLado = primerLado;
        this.segundoLado = segundoLado;
        this.tercerLado = tercerLado;
    }

    @Override
    public double calcularArea() {
        // Fórmula de Herón
        double s = calcularPerimetro() / 2;
        return Math.sqrt(s * (s - primerLado) * (s - segundoLado) * (s - tercerLado));
    }

    @Override
    public double calcularPerimetro() {
        return primerLado + segundoLado + tercerLado;
    }
}
