package labs.lab1.ActividadDos;

public class Rectangulo implements Figura {
    private double ancho;
    private double altura;

    // Constructor
    public Rectangulo(double ancho, double altura) {
        this.ancho = ancho;
        this.altura = altura;
    }

    @Override
    public double calcularArea() {
        return ancho * altura;
    }

    @Override
    public  double calcularPerimetro() {
        return 2 * ancho + 2 * altura;
    }

    @Override
    public String toString() {
        return String.format("Rectangle, area: %.2f perimeter: %.2f", calcularArea(), calcularPerimetro());
    }

}
