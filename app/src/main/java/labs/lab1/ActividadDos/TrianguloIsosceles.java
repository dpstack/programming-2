package labs.lab1.ActividadDos;

public class TrianguloIsosceles extends Triangulo {
    public TrianguloIsosceles(double base, double lado) {
        super(base, lado, lado);
    }

    @Override
    public String toString() {
        return String.format("Isosceles triangle, area: %.2f perimeter: %.2f", calcularArea(), calcularPerimetro());
    }
}
