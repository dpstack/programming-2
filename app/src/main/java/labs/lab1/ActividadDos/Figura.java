package labs.lab1.ActividadDos;

public interface Figura {
    double calcularArea();
    double calcularPerimetro();
}
