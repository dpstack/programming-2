package labs.lab1.ActividadDos;

public class Cuadrado extends Rectangulo {
    public Cuadrado(double lado) {
        super(lado, lado);
    }

    @Override
    public String toString() {
        return String.format("Square, area: %.2f perimeter: %.2f", calcularArea(), calcularPerimetro());
    }
}
