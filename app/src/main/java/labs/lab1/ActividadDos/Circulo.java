package labs.lab1.ActividadDos;

public class Circulo implements Figura {
    private double radio;

    // Constructor
    public Circulo(double radio) {
        this.radio = radio;
    }

    @Override
    public double calcularArea() {
        return Math.PI * Math.pow(radio, 2);
    }

    @Override
    public double calcularPerimetro() {
        return 2 * Math.PI * radio;
    }

    @Override
    public String toString() {
        return String.format("Circle, area: %.3f perimeter: %.3f", calcularArea(), calcularPerimetro());
    }

}
