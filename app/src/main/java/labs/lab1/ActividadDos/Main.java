package labs.lab1.ActividadDos;

public class Main {
    public static void main(String[] args) {

        Circulo circle = new Circulo(10);
        System.out.println(circle);

        Rectangulo rectangle = new Rectangulo(15, 7);
        System.out.println(rectangle);

        TrianguloEquilatero equilateralTriangle = new TrianguloEquilatero(7);
        System.out.println(equilateralTriangle);

        TrianguloIsosceles isoscelesTriangle = new TrianguloIsosceles(4, 7);
        System.out.println(isoscelesTriangle);

        TrianguloEscaleno scaleneTriangle = new TrianguloEscaleno(4, 7, 9);
        System.out.println(scaleneTriangle);

        Cuadrado square = new Cuadrado(2);
        System.out.println(square);



    }
}
