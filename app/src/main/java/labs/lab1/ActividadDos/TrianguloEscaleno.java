package labs.lab1.ActividadDos;

public class TrianguloEscaleno extends Triangulo {
    public TrianguloEscaleno(double primerLado, double segundoLado, double tercerLado) {
        super(primerLado, segundoLado, tercerLado);
    }

    @Override
    public String toString() {
        return String.format("Scalene triangle, area: %.2f perimeter: %.2f", calcularArea(), calcularPerimetro());
    }
}
