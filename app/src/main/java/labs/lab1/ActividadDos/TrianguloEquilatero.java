package labs.lab1.ActividadDos;

public class TrianguloEquilatero extends Triangulo {
    public TrianguloEquilatero (double lado) {
        super(lado, lado, lado);
    }

    @Override
    public String toString() {
        return String.format("Equilateral triangle, area: %.2f perimeter: %.2f", calcularArea(), calcularPerimetro());
    }
}
