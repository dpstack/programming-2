package labs.lab1.ActividadUno;

import java.util.ArrayList;
import java.util.Scanner;

public class ActividadUno {

    private static int factorial(int numero) {
        int resultado = 1;
        for (int i = numero; i > 1; i--) {
            resultado *= i;
        }

        return resultado;
    }

    public static char ultimoDigito(int resultado) {
        String numero = String.valueOf(resultado);
        return numero.charAt(numero.length() - 1);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("EJERCICIO 1 - QUADRANT SELECTION");
        // System.out.print("Ingrese el valor de la coordenada en x: ");
        double x = sc.nextDouble();

        // System.out.print("Ingrese el valor de la coordenada en y: ");
        double y = sc.nextDouble();

        if (x > -1000 && x < 1000 && x!=0 && y > -1000 && y < 1000 && y!=0) {
            // System.out.println("Está adentro del rango!");
            if (x < 0 && y > 0) {
                System.out.println("1");
            } else if (x > 0 && y > 0) {
                System.out.println("2");
            } else if (x < 0 && y < 0) {
                System.out.println("3");
            } else {
                System.out.println("4");
            }
        } else {
            //System.out.println("Está afuera del rango");
            System.out.println("?");
        }

        /*int a = factorial(3);
        int b = factorial(5);
        char u_a = ultimoDigito(a);
        char u_b = ultimoDigito(b);
        System.out.println(a + " " + u_a);
        System.out.println(b + " " + u_b);*/

        System.out.println("EJERCICIO 2 - LAST FACTORIAL DIGIT");
        //System.out.print("Ingrese la cantidad de casos de prueba (1 < T < 10): ");
        int T = sc.nextInt();

        while (T <= 1 || T >= 10) {
            //System.out.println("Ingrese una cantidad de casos de prueba válidad (1 < T < 10): ");
            T = sc.nextInt();
        }
        int N;
        ArrayList<Integer> entradas = new ArrayList<>();
        for (int i = 1; i <= T; i++) {
            //System.out.print("Ingrese un entero positivo N: ");
            N = sc.nextInt();
            while (N <= 0) {
                //System.out.print("Ingres un número entero positvo válido (Mayor a cero): ");
                N = sc.nextInt();
            }

            entradas.add(N);
        }

        for (int dato : entradas) {
            //System.out.println("Dato: " + dato + " Factorial: " + factorial(dato) + " último dígito factorial: " + ultimoDigito(factorial(dato)));
            System.out.println(ultimoDigito(factorial(dato)));
        }

        sc.close();

    }
}