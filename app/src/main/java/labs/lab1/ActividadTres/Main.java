package labs.lab1.ActividadTres;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        University jalaUniversity = new University();

        Student gery = new Student("Gery", 1, "induction");
        Student luis = new Student("Luis", 2, "maths", "science");
        Student raul = new Student("Raul", 2, "maths", "science");
        Student liz = new Student("Liz", 3, "database I", "science");

        jalaUniversity.addStudent(gery);
        jalaUniversity.addStudent(luis);
        jalaUniversity.addStudent(raul);
        jalaUniversity.addStudent(liz);

        // Adicionar curso a un estudiante
        jalaUniversity.addCourseToStudent(3, "database II");

        // Promover estudiante
        jalaUniversity.promoteStudents(raul);

        // Listar estudiantes
        List<String> scienceStudents = jalaUniversity.filterStudentsByCourse("science");
        System.out.println(scienceStudents);

        // Listar todos los estudiantes
        jalaUniversity.listAllStudents();




    }

}
