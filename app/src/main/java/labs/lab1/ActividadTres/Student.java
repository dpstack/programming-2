package labs.lab1.ActividadTres;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Student {
    private String name;
    private int grade;
    private List<String> courses;

    // Constructor
    public Student(String name, int grade, String... courses) {
        this.name = name;
        this.grade = grade;
        this.courses = new ArrayList<>(Arrays.asList(courses));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public void addCourse(String course) {
        courses.add(course);
    }

    @Override
    public String toString() {
        return String.format("Name: %s, Grade: %s, courses: %s", name, grade, String.join(", ", courses));
    }
}
