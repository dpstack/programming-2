package labs.lab1.ActividadTres;

import java.util.ArrayList;
import java.util.List;

public class University {
    private List<Student> students;

    // Constructor
    public University() {
        students = new ArrayList<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public void addCourseToStudent(int studentIndex, String course) {
        if (studentIndex < students.size() && studentIndex >= 0) {
            students.get(studentIndex).addCourse(course);
        }
    }

    public void promoteStudents(Student student) {
        student.setGrade(student.getGrade() + 1);
    }

    public List<String> filterStudentsByCourse(String course) {
        List<String> filteredStudents = new ArrayList<>();
        for (Student student : students) {
            if (student.getCourses().contains(course)) {
                filteredStudents.add(student.getName());
            }
        }

        return filteredStudents;

    }

    public void listAllStudents() {
        for (Student student : students) {
            System.out.println(student);
        }
    }

}
