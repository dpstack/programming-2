package labs.lab3.Interfaces;

import java.time.LocalDate;

public interface IPersona {
    String getNombre();
    String getApellido();
    LocalDate getFechaNacimiento();
    String getTelefono();
    String getEmail();
}
