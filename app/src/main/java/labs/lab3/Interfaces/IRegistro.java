package labs.lab3.Interfaces;

// Intefaz genérica, OCP
public interface IRegistro<T> {
    void agregar(T item);
}
