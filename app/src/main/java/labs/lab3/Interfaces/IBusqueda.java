package labs.lab3.Interfaces;
import java.util.Optional;

public interface IBusqueda<T> {
    Optional<T> buscarPorNombre(String nombre);
}
