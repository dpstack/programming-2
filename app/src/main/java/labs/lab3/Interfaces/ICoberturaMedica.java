package labs.lab3.Interfaces;

public interface ICoberturaMedica {

    boolean hayCobertura(String servicio);

}
