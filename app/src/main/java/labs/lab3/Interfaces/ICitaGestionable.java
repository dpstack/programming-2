package labs.lab3.Interfaces;

import java.time.LocalDateTime;
// Segregación de Interfaces
public interface ICitaGestionable {
    void cancelar();
    void reprogramar(LocalDateTime fechaReprogramada);
}
