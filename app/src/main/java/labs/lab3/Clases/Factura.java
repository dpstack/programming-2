package labs.lab3.Clases;

import java.time.LocalDateTime;

public class Factura {

    private final Cita cita;
    private final double monto;
    private final LocalDateTime fecha;
    private String estado;

    public Factura(Cita cita, double monto) {
        this.cita = cita;
        this.monto = monto;
        this.fecha = LocalDateTime.now();
        this.estado = "Pendiente";
    }

    public void pagar() {
        this.estado = "Pagada";
    }

    public Cita getCita() {
        return cita;
    }

    public double getMonto() {
        return monto;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public String getEstado() {
        return estado;
    }
}
