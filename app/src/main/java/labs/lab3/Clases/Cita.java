package labs.lab3.Clases;

import java.time.LocalDateTime;

import labs.lab3.Enums.EstadoCita;
import labs.lab3.Interfaces.ICitaGestionable;

public class Cita implements ICitaGestionable {

    private Paciente paciente;
    private Medico medico;
    private LocalDateTime fecha;
    private EstadoCita estado;

    public Cita(Paciente paciente, Medico medico, LocalDateTime fecha) {
        this.paciente = paciente;
        this.medico = medico;
        this.fecha = fecha;
        this.estado = EstadoCita.PROGRAMADA;
    }

    @Override
    public void cancelar() {
        this.estado = EstadoCita.CANCELADA;
    }

    @Override
    public void reprogramar(LocalDateTime fechaReprogramada) {
        this.fecha = fechaReprogramada;
        this.estado = EstadoCita.PROGRAMADA;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public EstadoCita getEstado() {
        return estado;
    }
}
