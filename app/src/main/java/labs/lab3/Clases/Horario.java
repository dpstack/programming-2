package labs.lab3.Clases;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

import java.util.Map; // Intefaz
import java.util.EnumMap; // Implementación, permite hacer operaciones

import java.util.List; // Interfaz
import java.util.ArrayList; // Implementación, permite hacer operaciones

public class Horario {

    private Map<DayOfWeek, List<LocalTime>> disponibilidad;

    public Horario() {
        this.disponibilidad = new EnumMap<>(DayOfWeek.class);
    }

    public void setDisponibilidad(DayOfWeek dia, List<LocalTime> horas) {
        disponibilidad.put(dia, new ArrayList<>(horas));
    }

    public boolean isDisponible(LocalDateTime fechaHora) {
        DayOfWeek dia = fechaHora.getDayOfWeek();
        LocalTime hora = fechaHora.toLocalTime();
        return disponibilidad.containsKey(dia) && disponibilidad.get(dia).contains(hora);
    }

}
