package labs.lab3.Clases;

import java.util.List;

import labs.lab3.Abstractas.AbstractReporte;

public class ReporteCitas extends AbstractReporte {
    private ManejoCitas manejoCitas;

    public ReporteCitas(String titulo, ManejoCitas manejoCitas) {
        super(titulo);
        this.manejoCitas = manejoCitas;
    }

    @Override
    public String generarContenido() {
        StringBuilder contenido = new StringBuilder();
        contenido.append("Reporte de Citas\n");
        contenido.append("================\n\n");

        List<Cita> todasLasCitas = manejoCitas.obtenerTodasLasCitas();

        contenido.append("Total de citas: ").append(todasLasCitas.size()).append("\n\n");

        for (Cita cita : todasLasCitas) {
            contenido.append("Fecha: ").append(cita.getFecha()).append("\n");
            contenido.append("Paciente: ").append(cita.getPaciente().getNombre()).append(" ")
                    .append(cita.getPaciente().getApellido()).append("\n");
            contenido.append("Médico: ").append(cita.getMedico().getNombre()).append(" ")
                    .append(cita.getMedico().getApellido()).append("\n");
            contenido.append("Especialidad: ").append(cita.getMedico().getEspecialidad()).append("\n");
            contenido.append("Estado: ").append(cita.getEstado()).append("\n\n");
        }

        return contenido.toString();
    }

}
