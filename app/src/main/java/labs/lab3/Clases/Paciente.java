package labs.lab3.Clases;

import java.time.LocalDate;

import labs.lab3.Abstractas.AbstractPersonaBase;

public class Paciente extends AbstractPersonaBase {
    // LSP - Liskov
    private String seguro;
    private HistorialMedico historialMedico;

    public Paciente(String nombre, String apellido, LocalDate fechaNacimiento, String telefono, String email, String seguro) {
        super(nombre, apellido, fechaNacimiento, telefono, email);
        this.seguro = seguro;
        this.historialMedico = new HistorialMedico();
    }

    public String getSeguro() {
        return seguro;
    }

    public HistorialMedico getHistorialMedico() {
        return historialMedico;
    }
}
