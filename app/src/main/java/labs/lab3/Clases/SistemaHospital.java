package labs.lab3.Clases;

import labs.lab3.Abstractas.AbstractServicioMedico;
import labs.lab3.Enums.Especialidad;
import labs.lab3.Abstractas.AbstractReporte;
import labs.lab3.Excepciones.CoberturaInsuficienteException;
import labs.lab3.Excepciones.MedicoNoDisponibleException;
import labs.lab3.Excepciones.PacienteNoEncontradoException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SistemaHospital {
    private ManejoPacientes manejoPacientes;
    private ManejoMedicos manejoMedicos;
    public ManejoCitas manejoCitas;
    public Facturacion facturacion;
    private List<AbstractServicioMedico> servicios;
    private List<AbstractReporte> reportes;

    public SistemaHospital() {
        this.manejoPacientes = new ManejoPacientes();
        this.manejoMedicos = new ManejoMedicos();
        this.manejoCitas = new ManejoCitas();
        this.facturacion = new Facturacion();
        this.servicios = new ArrayList<>();
        this.reportes = new ArrayList<>();
    }

    public void registrarPaciente(Paciente paciente) {
        manejoPacientes.agregar(paciente);
    }

    public void registrarMedico(Medico medico) {
        manejoMedicos.agregar(medico);
    }

    public Cita programarCita(String nombrePaciente, String nombreMedico, LocalDateTime fecha, SeguroMedico seguro) 
            throws PacienteNoEncontradoException, MedicoNoDisponibleException, CoberturaInsuficienteException {
        Paciente paciente = manejoPacientes.buscarPorNombre(nombrePaciente)
                .orElseThrow(() -> new PacienteNoEncontradoException(nombrePaciente));
        Medico medico = manejoMedicos.buscarPorNombre(nombreMedico)
                .orElseThrow(() -> new PacienteNoEncontradoException(nombreMedico));
        
        return manejoCitas.programarCita(paciente, medico, fecha, seguro);
    }

    public boolean cancelarCita(Cita cita) {
        return manejoCitas.cancelarCita(cita);
    }

    public Factura generarFactura(Cita cita, double monto) {
        return facturacion.generarFactura(cita, monto);
    }

    public void agregarServicio(AbstractServicioMedico servicio) {
        servicios.add(servicio);
    }

    public void generarReporte(AbstractReporte reporte) {
        reportes.add(reporte);
    }

    public List<Paciente> buscarPacientesPorSeguro(String seguro) {
        return manejoPacientes.buscarPorSeguro(seguro);
    }

    public List<Medico> buscarMedicosPorEspecialidad(Especialidad especialidad) {
        return manejoMedicos.buscarPorEspecialidad(especialidad);
    }

    public List<Cita> buscarCitasPaciente(Paciente paciente) {
        return manejoCitas.buscarCitasPaciente(paciente);
    }

    public void agregarEntradaHistorialMedico(Paciente paciente, EntradaHistorial entrada) {
        paciente.getHistorialMedico().addEntrada(entrada);
    }

    public List<EntradaHistorial> obtenerHistorialMedico(Paciente paciente) {
        return paciente.getHistorialMedico().getHistorial();
    }

    public List<AbstractServicioMedico> getServicios() {
        return servicios;
    }

    public List<AbstractReporte> getReportes() {
        return reportes;
    }
}
