package labs.lab3.Clases;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import labs.lab3.Enums.Especialidad;
import labs.lab3.Abstractas.AbstractManejo;

public class ManejoMedicos extends AbstractManejo<Medico> {

    private List<Medico> medicos;

    public ManejoMedicos() {
        this.medicos = new ArrayList<>();
    }

    @Override
    public void agregar(Medico medico) {
        medicos.add(medico);
    }


    @Override
    public Optional<Medico> buscarPorNombre(String nombre) {
        return medicos.stream()
               .filter(med -> med.getNombre().equalsIgnoreCase(nombre))
               .findFirst();
    }

    public List<Medico> buscarPorEspecialidad(Especialidad especialidad) {
        return buscar(med -> med.getEspecialidad() == especialidad);
    }


}
