package labs.lab3.Clases;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import labs.lab3.Abstractas.AbstractManejo;

public class ManejoPacientes extends AbstractManejo<Paciente> {

    private Map<String, Paciente> pacientes;

    public ManejoPacientes() {
        this.pacientes = new HashMap<>();
    }

    // Filtrar por seguro
    @Override
    public void agregar(Paciente paciente) {
        pacientes.put(paciente.getSeguro(), paciente);
    }

    
    @Override
    public Optional<Paciente> buscarPorNombre(String nombre) {
        return pacientes.values().stream()
               .filter(pac -> pac.getNombre().equalsIgnoreCase(nombre))
               .findFirst();
    }

    public List<Paciente> buscarPorSeguro(String seguro) {
        return buscar(pac -> pac.getSeguro().equalsIgnoreCase(seguro));
    }
    
}
