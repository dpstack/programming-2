package labs.lab3.Clases;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import labs.lab3.Excepciones.CoberturaInsuficienteException;
import labs.lab3.Excepciones.MedicoNoDisponibleException;
import labs.lab3.Excepciones.PacienteNoEncontradoException;

public class ManejoCitas {

    private List<Cita> citas;

    public ManejoCitas() {
        this.citas = new ArrayList<>();
    }

    public Cita programarCita(Paciente paciente, Medico medico, LocalDateTime fecha, SeguroMedico seguro) 
            throws MedicoNoDisponibleException, PacienteNoEncontradoException, CoberturaInsuficienteException {
        if (paciente == null) {
            throw new PacienteNoEncontradoException("null");
        }
        if (medico == null) {
            throw new PacienteNoEncontradoException("null"); 
        }

        if (seguro != null && !seguro.hayCobertura(medico.getEspecialidad().toString())) {
            throw new CoberturaInsuficienteException("El seguro del paciente no cubre la especialidad del médico.");
        } 

        if (medico.getHorario().isDisponible(fecha)) {
            Cita cita = new Cita(paciente, medico, fecha);
            citas.add(cita);
            return cita;
        } else {
            throw new MedicoNoDisponibleException(medico, fecha);
        }
    }

    public List<Cita> obtenerTodasLasCitas() {
        return new ArrayList<>(citas);
    }

    public List<Cita> buscarCitasPaciente(Paciente paciente) {
        return citas.stream()
                .filter(cita -> cita.getPaciente().equals(paciente))
                .collect(Collectors.toList());
    }

    public boolean cancelarCita(Cita cita) {
        if (citas.contains(cita)) {
            cita.cancelar();
            return true;
        }
        return false;
    }
}
