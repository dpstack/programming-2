package labs.lab3.Clases;

import java.util.HashSet;
import java.util.Set;

import labs.lab3.Interfaces.ICoberturaMedica;

public class SeguroMedico implements ICoberturaMedica {

    private String nombre;
    private Set<String> cobertura;

    public SeguroMedico(String nombre, Set<String> cobertura) {
        this.nombre = nombre;
        this.cobertura = new HashSet<>(cobertura);
    }

    @Override
    public boolean hayCobertura(String servicio) {
        return cobertura.contains(servicio);
    }

    public String getNombre() {
        return nombre;
    }
}
