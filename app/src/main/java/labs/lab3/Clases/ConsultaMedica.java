package labs.lab3.Clases;

import java.time.Duration;

import labs.lab3.Abstractas.AbstractServicioMedico;

public class ConsultaMedica extends AbstractServicioMedico {
    public ConsultaMedica(String nombre, double costo, Duration duracion) {
        super(nombre, costo, duracion);
    }

    @Override
    public void realizarServicio() {
        System.out.println("Realizando consulta médica: " + this.nombre);
        System.out.println("Duración: " + this.duracion.toMinutes() + " minutos");
        System.out.println("Costo: $" + this.costo);
        System.out.println("1. Revisar síntomas del paciente");
        System.out.println("2. Realizar examen físico");
        System.out.println("3. Diagnosticar y recetar tratamiento");
        System.out.println("4. Programar seguimiento si es necesario");
        System.out.println("Consulta médica completada.");
    }

}
