package labs.lab3.Clases;

import java.time.LocalDate;

import labs.lab3.Abstractas.AbstractPersonaBase;
import labs.lab3.Enums.Especialidad;

// Herencia
public class Medico extends AbstractPersonaBase {

    private Especialidad especialidad;
    private Horario horario;

    public Medico(String nombre, String apellido, LocalDate fechaNacimiento, String telefono, String email, Especialidad especialidad) {
        super(nombre, apellido, fechaNacimiento, telefono, email);
        this.especialidad = especialidad;
        this.horario = new Horario();
    }

    public Especialidad getEspecialidad() {
        return especialidad;
    }

    public Horario getHorario() {
        return horario;
    }
}
