package labs.lab3;

import labs.lab3.Clases.*;
import labs.lab3.Enums.*;
import labs.lab3.Excepciones.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.DayOfWeek;
import java.time.Duration;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        SistemaHospital sistema = new SistemaHospital();

        // Crear y registrar pacientes
        Paciente pacienteDaniel = new Paciente("Daniel", "Pérez", LocalDate.of(2000, 5, 15), "257685228", "danp@email.com", "Seguro A");
        Paciente pacienteGustavo = new Paciente("Gustavo", "Rodríguez", LocalDate.of(2010, 2, 1), "4636333", "gustav@email.com", "Seguro B");
        sistema.registrarPaciente(pacienteDaniel);
        sistema.registrarPaciente(pacienteGustavo);

        // Crear y registrar médicos
        Medico medicoMiguel = new Medico("Dr. Miguel", "Montalvo", LocalDate.of(1990, 3, 10), "847922404", "mgmv@areandina.co", Especialidad.CARDIOLOGIA);
        Medico medicoLuis = new Medico("Dra. Luis", "Martínez", LocalDate.of(1975, 11, 5), "19071408", "lsxt@areandina.co", Especialidad.PEDIATRIA);
        sistema.registrarMedico(medicoMiguel);
        sistema.registrarMedico(medicoLuis);

        // Configurar horarios de médicos
        medicoMiguel.getHorario().setDisponibilidad(DayOfWeek.MONDAY, Arrays.asList(LocalTime.of(9, 0), LocalTime.of(10, 0), LocalTime.of(11, 0)));
        medicoLuis.getHorario().setDisponibilidad(DayOfWeek.TUESDAY, Arrays.asList(LocalTime.of(14, 0), LocalTime.of(15, 0), LocalTime.of(16, 0)));

        // Crear seguro médico
        Set<String> cobertura = new HashSet<>(Arrays.asList("CARDIOLOGIA", "PEDIATRIA"));
        SeguroMedico seguro = new SeguroMedico("Seguro A", cobertura);

        try {
            // Programar citas
            Cita primerCita = sistema.programarCita(pacienteDaniel.getNombre(), medicoMiguel.getNombre(), LocalDateTime.of(2024, 7, 15, 9, 0), seguro);
            Cita segundaCita = sistema.programarCita(pacienteGustavo.getNombre(), medicoLuis.getNombre(), LocalDateTime.of(2024, 7, 16, 16, 0), seguro);

            // Generar facturas
            Factura primeraFactura = sistema.generarFactura(primerCita, 100.0);
            Factura segundaFactura = sistema.generarFactura(segundaCita, 150.0);

            // Agregar entradas al historial médico
            sistema.agregarEntradaHistorialMedico(pacienteDaniel, new EntradaHistorial(LocalDate.now(), "Consulta General", medicoMiguel));
            sistema.agregarEntradaHistorialMedico(pacienteGustavo, new EntradaHistorial(LocalDate.now(), "Vacunación", medicoLuis));

            // Agregar servicios médicos
            sistema.agregarServicio(new ConsultaMedica("Consulta General", 100.0, Duration.ofMinutes(30)));
            sistema.agregarServicio(new ConsultaMedica("Consulta Especializada", 150.0, Duration.ofMinutes(45)));

            // Generar reportes
            sistema.generarReporte(new ReporteCitas("Reporte de Citas Diarias", sistema.manejoCitas));

            // Mostrar resultados
            System.out.println("Citas programadas: " + sistema.buscarCitasPaciente(pacienteDaniel).size());
            System.out.println("Facturas generadas: " + sistema.facturacion.getFacturas());
            System.out.println("Entradas en historial médico de Juan: " + sistema.obtenerHistorialMedico(pacienteDaniel).size());
            System.out.println("Servicios médicos disponibles: " + sistema.getServicios().size());
            System.out.println("Reportes generados: " + sistema.getReportes().size());

        } catch (PacienteNoEncontradoException | MedicoNoDisponibleException | CoberturaInsuficienteException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
