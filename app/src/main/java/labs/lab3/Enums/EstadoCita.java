package labs.lab3.Enums;

public enum EstadoCita {
    PROGRAMADA,
    CONFIRMADA,
    CANCELADA,
    REALIZADA,
}
