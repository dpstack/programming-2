package labs.lab3.Enums;

public enum Especialidad {
    CARDIOLOGIA,
    DERMATOLOGIA,
    PEDIATRIA,
    TRAUMATOLOGIA,
}
