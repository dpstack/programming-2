package labs.lab3.Abstractas;

import java.time.Duration;

public abstract class AbstractServicioMedico {
    protected String nombre;
    protected double costo;
    protected Duration duracion;

    public AbstractServicioMedico(String nombre, double costo, Duration duracion) {
        this.nombre = nombre;
        this.costo = costo;
        this.duracion = duracion;
    }

    public abstract void realizarServicio();

    public String getNombre() {
        return nombre;
    }

    public double getCosto() {
        return costo;
    }

    public Duration getDuracion() {
        return duracion;
    }
}
