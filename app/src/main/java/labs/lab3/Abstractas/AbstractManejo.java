package labs.lab3.Abstractas;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import labs.lab3.Interfaces.IBusqueda;
import labs.lab3.Interfaces.IPersona;
import labs.lab3.Interfaces.IRegistro;

public abstract class AbstractManejo<T extends IPersona> implements IRegistro<T>, IBusqueda<T> {

    protected List<T> elementos;

    public AbstractManejo() {
        this.elementos = new ArrayList<>();
    }

    @Override
    public void agregar(T item) {
        elementos.add(item);
    }

    @Override
    public Optional<T> buscarPorNombre(String nombre) {
        return elementos.stream()
                .filter(e -> e.getNombre().equalsIgnoreCase(nombre))
                .findFirst();
    }

    public List<T> buscar(Predicate<T> predicate) {
        return elementos.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
