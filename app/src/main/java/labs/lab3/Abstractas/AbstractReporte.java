package labs.lab3.Abstractas;

import java.time.LocalDate;

public abstract class AbstractReporte {
    protected String titulo;
    protected LocalDate fechaGeneracion;

    public AbstractReporte(String titulo) {
        this.titulo = titulo;
        this.fechaGeneracion = LocalDate.now();
    }

    public abstract String generarContenido();

    public String getTitulo() {
        return titulo;
    }

    public LocalDate getFechaGeneracion() {
        return fechaGeneracion;
    }

    public final String generarReporte() {
        return "Reporte: " + titulo + "\n" +
               "Fecha de generación: " + fechaGeneracion + "\n\n" +
               generarContenido();
    }
}
