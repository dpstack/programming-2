package labs.lab3.Excepciones;

public class CoberturaInsuficienteException extends Exception {

    public CoberturaInsuficienteException(String message) {
        super(message);
    }
    
}
