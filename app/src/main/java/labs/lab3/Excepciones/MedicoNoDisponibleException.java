package labs.lab3.Excepciones;

import java.time.LocalDateTime;

import labs.lab3.Clases.Medico;

public class MedicoNoDisponibleException extends Exception {

    public MedicoNoDisponibleException(Medico medico, LocalDateTime fecha) {
        super("El medico " + medico.getNombre() + " " + medico.getApellido()
         + " no esta disponible para la fecha " + fecha);
    }
    
}
