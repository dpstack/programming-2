package labs.lab3.Excepciones;

public class PacienteNoEncontradoException extends Exception {

    public PacienteNoEncontradoException(String seguro) {
        super("Paciente con seguro " + seguro + " no encontrado");
    }
    
}
